"use strict"


//перше завдання
const a = document.createElement('a');
const footer = document.querySelector("footer");
a.textContent = "Learn More";
a.setAttribute("href", "#");
footer.append(a);


//друге завдання
const select = document.createElement('select');
const main = document.querySelector("main");

select.setAttribute("id", "rating");
main.prepend(select);


const optionText = ["1 Stars", "2 Stars","3 Stars","4 Stars"];
const optionArr = ["1", "2", "3", "4"];

function createElements(){
    const rating = document.getElementById("rating");

    optionText.forEach((text, index) =>{
        const option = document.createElement("option");
        option.value = optionArr[index];
        option.textContent = text;
        rating.appendChild(option);

    })
}

createElements();

